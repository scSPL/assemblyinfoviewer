﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;

namespace AssemblyInfoViewer
{
    public class Program
    {
        static void Main(string[] args)
        {
            var filepath = args[0];
            var directoryPath = args[1];
            var fileName = directoryPath + "/" + Guid.NewGuid() + ".txt";

            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }

            Assembly assembly;
            try
            {
                assembly = Assembly.LoadFrom(filepath);
            }
            catch (Exception e)
            {
                using (var sw = new StreamWriter(fileName))
                {
                    sw.WriteLine("Can't load assembly :(");
                }

                Process.Start("notepad.exe", fileName);
                return;
            }

            var attributes = assembly.CustomAttributes;

            using (var sw = new StreamWriter(fileName))
            {
                sw.WriteLine(filepath);

                var assemblyName = assembly.GetName();

                sw.WriteLine();
                sw.WriteLine($"{assemblyName.Name}, Version={assemblyName.Version}");
                sw.WriteLine();

                List<string> attributesList = new List<string>();

                foreach (CustomAttributeData attribute in attributes)
                {
                    List<string> argumentValues = new List<string>();
                    if (attribute.ConstructorArguments.Count != 0)
                    {
                        foreach (var argument in attribute.ConstructorArguments)
                        {
                            argumentValues.Add(argument.ToString());
                        }
                    }

                    if (attribute.NamedArguments.Count != 0)
                    {
                        foreach (var argument in attribute.NamedArguments)
                        {
                            argumentValues.Add(argument.ToString());
                        }
                    }

                    var innerValue = (argumentValues.Count > 0) ?
                        argumentValues.Aggregate((str1, str2) => str1 + ", " + str2) : string.Empty;

                    var attributeName =
                        attribute.AttributeType.Name.Substring(0,
                            attribute.AttributeType.Name.Length - "Attribute".Length);
                    attributesList.Add("[assembly: " + attributeName + "(" + innerValue + ")" + "]");
                }

                attributesList.Sort();

                foreach (var attribute in attributesList)
                {
                    sw.WriteLine(attribute);
                }
            }

            Process.Start("notepad.exe", fileName);
        }
    }
}
