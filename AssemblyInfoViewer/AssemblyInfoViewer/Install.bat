@ECHO off
IF NOT EXIST "%CD%\AssemblyInfoViewer.exe" ECHO File "%CD%\AssemblyInfoViewer.exe" should exist. Cannot complete operation.
IF NOT EXIST "%CD%\AssemblyInfoViewer.exe" PAUSE
IF NOT EXIST "%CD%\AssemblyInfoViewer.exe" exit

REG ADD "hkcu\Software\Classes\dllfile\shell\View assembly info" /ve /t REG_SZ /d "View assembly info" /f
REG ADD "hkcu\Software\Classes\dllfile\shell\View assembly info\command" /ve /t REG_SZ /d "%CD%\AssemblyInfoViewer.exe \"%%1\" c:\Garbage" /f

ECHO Context menu handlers were added successfully.
PAUSE